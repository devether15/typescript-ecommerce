import './App.css';
import { Home } from './pages/home/Home';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
// import { color } from './themes/colorPalette';
// import { Navbar } from './components/navigation/navbar/Navbar';
// import { AuthRouter } from './routes/AuthRouter';

console.log("color")

function App() {
  return (
    <Router>
      {/* <Navbar /> */}
      <Switch>
        <Route exact path="/" component={Home} />
        {/* <Route path="/auth" component={AuthRouter} /> */}
      </Switch>
    </Router>
  );
}

export default App;
