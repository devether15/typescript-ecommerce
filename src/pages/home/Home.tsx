import React from 'react'
import { Carrousel } from '../../components/carrousel/Carrousel'
import { PageLayout } from '../../components/page-layout/PageLayout'

interface HomeProps {
    Carrousel: React.Component,
    PageLayout:  React.Component,
}

export const Home:HomeProps = () => {
    return (    
        <PageLayout>
            <Carrousel/>
        </PageLayout>
    )
}
